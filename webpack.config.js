const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const path = require('path');

module.exports = env => {
  const MODE = env.NODE_ENV;

  const plugins = [
    new HtmlWebpackPlugin({
      template: './frontend/public/index.html'
    })
  ]

  let optimization = {}

  if(MODE === 'production') {
    plugins.push(new MiniCssExtractPlugin());

    optimization = {
      minimizer: [
        new CssMinimizerPlugin()
      ]
    }
  }

  const js = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: "babel-loader"
  }

  const css = {
    test: /\.css$/,
    use: [
      MODE === 'development' ? "style-loader" : MiniCssExtractPlugin.loader,
      "css-loader"
    ]
  }

  const assets = {
    test: /\.(jpg|jpeg|gif|png|svg)$/,
    use: [
      MODE === 'development' ? {
        loader: "url-loader"
      } : {
        loader: "file-loader",
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  }

  return{
    entry: './frontend/index.js',
    output: {
      path: path.resolve(__dirname, 'backend/public'),
      filename: 'bundle.js'
    },
    module:{ 
      rules: [
        js, css, assets
      ]
    },
    plugins: plugins,

    optimization: optimization,

    devServer: {
      host: 'localhost',
      port: 3000,
      historyApiFallback: true,
    }
  }
}