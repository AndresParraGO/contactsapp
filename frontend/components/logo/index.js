import React from 'react';
import { Link } from 'wouter';
import './index.css';
import LogoAsset from '../../assets/logo.png';

function Logo({ center }) {
  return (
    <article className={`${center && 'center'}`}>
      <Link to="/"><img src={LogoAsset} alt="Logo of contactsapp" /></Link>
    </article>
  );
}

export default Logo;
