const API = process.env.NODE_ENV === 'development' ? 'http://localhost:9000/api' : '/api';

export {
  API
}