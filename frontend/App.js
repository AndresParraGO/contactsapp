import React from 'react';
import { Route } from 'wouter';

import Home from './pages/home';
import Login from './pages/login';
import Register from './pages/register';

function App() {
  return <>
    <Route path="/" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
  </>
}

export default App;